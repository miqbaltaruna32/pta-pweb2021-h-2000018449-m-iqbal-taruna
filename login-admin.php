<?php 
    if(isset($_POST["submit"])){
        if($_POST["username"] == "admin" && $_POST["pass"] == 12345){
            header("Location: halaman-admin.php");
        } else {
            $error = true;
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/login-style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <title>Login Admin</title>
</head>
<body>
    <header class="header">
        <h1 class="title">Admin Login</h1>
    </header>
    <div class="form">
        <form action="" method="POST">
            <?php if(isset($_POST["submit"]) && $error = true) : ?>
                <p class="error">username atau password salah</p>
            <?php endif; ?>
            <input type="text" id="username" name="username" placeholder="Username" required>
            <input type="password" id="pass" name="pass" placeholder="Password" required>
            <button type="submit" name="submit">Login</button>
        </form>
    </div>
</body>
</html>