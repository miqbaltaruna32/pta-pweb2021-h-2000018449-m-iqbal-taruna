// navbar scrool efect
const header = document.querySelector(".header");
const hero = document.querySelector(".hero");
window.onscroll = () => {
    if (window.scrollY > hero.offsetHeight - 70) {
        header.style.background = "var(--green-gradient)"
    } else {
        header.style.background = "none";
    }
};

// project info and button
const projects = document.querySelectorAll(".project img");

projects.forEach(element => {
    element.addEventListener("mouseover", function () {
        element.nextElementSibling.style.display = "flex";
    })
    element.nextElementSibling.addEventListener("mouseout", function () {
        element.nextElementSibling.style.display = "none";
    })
});