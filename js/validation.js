const form = document.querySelector("form");
// pattern
const patt1 = /[a-z, A-Z]/g;
const patt2 = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/g;

const nama = document.getElementById("nama");
const email = document.getElementById("email");
const phone = document.getElementById("phone");
const pesan = document.getElementById("pesan");

function cekNama(element) {
    let aftrFilter = nama.value.match(patt1);
    if (aftrFilter == null || aftrFilter.length < nama.value.length) {
        nama.nextElementSibling.style.color = "#f44336";
        element.preventDefault();
    }
}

function cekEmail(element) {
    if (email.value.match(patt2)) return true;
    else {
        element.preventDefault();
        email.nextElementSibling.style.color = "#f44336";
        return false;
    }
}

function cekPhone(element) {
    if (phone.value.length < 8 || phone.value.length > 12) {
        element.preventDefault();
        phone.nextElementSibling.style.color = "#f44336"
    }
}

function cekPesan(element) {
    if (pesan.value.length < 15) {
        element.preventDefault();
        pesan.nextElementSibling.style.color = "#f44336";
    }
}

form.addEventListener("submit", function (e) {
    cekNama(e);
    cekEmail(e);
    cekPhone(e);
    cekPesan(e);
})