<?php 
// open all file
$fileNama = fopen("data/nama.txt", "r");
$fileEmail = fopen("data/email.txt", "r");
$filePhone = fopen("data/phone.txt", "r");
$filePesan = fopen("data/pesan.txt", "r");

// array 
$arrNama = array();
$arrEmail = array();
$arrPhone = array();
$arrPesan = array();

function inputArray($namaFile, $namaArray){
    $i = 0;
    while(!feof($namaFile)) {
        $namaArray[$i] = fgets($namaFile);
        // echo $namaArray[$i] . "<br>";
        $i = $i + 1;
    }
    return $namaArray;
}

$arrNama =  inputArray($fileNama, $arrNama);
$arrEmail =  inputArray($fileEmail, $arrEmail);
$arrPhone =  inputArray($filePhone, $arrPhone);
$arrPesan =  inputArray($filePesan, $arrPesan);

// close all file
fclose($fileNama);
fclose($fileEmail);
fclose($filePhone);
fclose($filePesan);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/admin-style.css">
    <title>Halaman Admin</title>
</head>
<body>
    <header class="header">
        <h1>Halaman Admin</h1>
    </header>
    <main class="container">
        <h2>Data Pengunjung</h2>
        <table>
            <thead>
                <th class="head1">No</th>
                <th class="head2">Nama</th>
                <th class="head2">Email</th>
                <th class="head2">No. Hp</th>
                <th class="head3">Pesan</th>
            </thead>
            <tbody>
                <?php for($i = 0; $i < count($arrEmail) - 1; $i++) : ?>
                    <tr>
                        <td><?= $i + 1 ?></td>
                        <td><?= $arrNama[$i] ?></td>
                        <td><?= $arrEmail[$i] ?></td>
                        <td><?= $arrPhone[$i] ?></td>
                        <td><?= $arrPesan[$i] ?></td>
                    </tr>
                <?php endfor; ?>
            </tbody>
        </table>
    </main>
    <script>
        const tableNo = document.querySelectorAll("tbody tr");
        tableNo.forEach(function(el){
            el.firstElementChild.style.textAlign = "center";
        })
    </script>
</body>
</html>